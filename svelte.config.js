import adapter from '@sveltejs/adapter-static';
import preprocess from 'svelte-preprocess';

/** @type {import('@sveltejs/kit').Config} */
const config = {
	// Consult https://github.com/sveltejs/svelte-preprocess
	// for more information about preprocessors
	preprocess: preprocess({
		scss: {
			prependData: `@import './src/lib/style/_variables.scss';`
		},
	}),

	kit: {
		adapter: adapter(),
	},

	package: {
		dir: 'package',
		emitTypes: true,
		exports: (filepath) => {
			return !/^.{0,}\/components\/docs\/.{0,}$/g.test(filepath)
		},
		files: (filepath) => {
			return !/^.{0,}\/components\/docs\/.{0,}$/g.test(filepath)
		},
	}
};

export default config;
