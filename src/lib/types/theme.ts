export enum ThemeEnum {
	LIGHT = 'theme-light',
	DARK = 'theme-dark',
	PURE_DARK = 'theme-pure-dark',
	PURE_LIGHT = 'theme-pure-light'
}
