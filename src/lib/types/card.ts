export enum CardVariant {
	STANDARD = 'standard',
	GLASS = 'glass'
}

export enum CardStatus {
	STANDARD = 'standard',
	ACTIVE = 'active'
}
