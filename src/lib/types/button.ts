export enum ButtonVariant {
	STANDARD = 'standard',
	PRIMARY = 'primary',
	HYPERLINK = 'hyperlink'
}

export enum ButtonStatus {
	STANDARD = 'standard',
	ACTIVE = 'active'
}

export enum ButtonState {
	ENABLED = 'enabled',
	DISABLED = 'disabled'
}
