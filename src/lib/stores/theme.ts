import { Store, type StoreChildConstructorOptions } from '$lib/classes/Store';
import { ThemeEnum } from '$lib/types/theme';

export class ThemeStore extends Store {
	public themes: ThemeEnum[];
	private listeningForBrowserPreferenceUpdate = false;

	constructor(options: StoreChildConstructorOptions) {
		super({
			defaultStateValue: options.defaultState,
			storeName: 'theme'
		});

		this.themes = Object.values(ThemeEnum);
	}

	public switchToNextTheme = () => {
		const currentThemeIndex = this.themes.indexOf(this.get().value);
		const nextThemeIndex = (currentThemeIndex + 1) % this.themes.length;
		const nextTheme = this.themes[nextThemeIndex];
		this.set(nextTheme);
	};

	public resetToInitial = (): void => {
		this.reset();
	};

	public syncWithBrowserPreference(options: { overrideSaved: boolean }) {
		const currentState = this.get();

		if (!this.listeningForBrowserPreferenceUpdate) {
			this.listenForBrowserPreferenceUpdate();
		}

		if (!currentState.useDefault && !options.overrideSaved) {
			return;
		}

		const darkModeMediaQuery = window.matchMedia('(prefers-color-scheme: dark)');

		if (darkModeMediaQuery.matches) {
			this.state.set({
				value: ThemeEnum.DARK,
				useDefault: true
			});
		} else {
			this.state.set({
				value: ThemeEnum.LIGHT,
				useDefault: true
			});
		}
	}

	private listenForBrowserPreferenceUpdate() {
		const darkModeMediaQuery = window.matchMedia('(prefers-color-scheme: dark)');
		darkModeMediaQuery.addEventListener('change', () => {
			this.syncWithBrowserPreference({ overrideSaved: false });
		});
		this.listeningForBrowserPreferenceUpdate = true;
	}

	public syncHtmlClass() {
		this.state.subscribe((state) => {
			document.documentElement.classList.remove(...this.themes);
			document.documentElement.classList.add(state.value);
		});
	}

	public static themeNameToIcon(name: ThemeEnum) {
		let icon: string;

		switch (name) {
			case ThemeEnum.DARK:
				icon = '🌙';
				break;

			case ThemeEnum.LIGHT:
				icon = '☀️';
				break;

			default:
				icon = '🎨';
				break;
		}

		return icon;
	}
}
